<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Router extends CI_Router {
	
	/**
	 * @var CI_Config
	 */
	public $config;
	
	/**
	 * @var CI_Lang
	 */
	public $lang;
	
	/**
	 * In addition to the original code we loads the CI_Lang class and the
	 * iso_639_1 configuration file.
	 * 
	 * @return void
	 */
	public function __construct()
	{
		$this->config =& load_class('Config', 'core');
		$this->lang   =& load_class('Lang',   'core');
		
		$this->config->load('iso_639_1');
		
		parent::__construct();
	}
	
	/**
	 * Before executing the original code we check if a valid language has been chosen and then
	 * store it inside the $user_language variable of the MY_Lang class.
	 * 
	 * @return void
	 */
	protected function _set_routing()
	{
		if ($this->uri->uri_string !== '')
		{
			if (array_key_exists($this->uri->segments[1], $this->config->item('iso_639_1')) AND
				file_exists(APPPATH . 'language/' . $this->config->item('iso_639_1')[$this->uri->segments[1]]))
			{
				$this->lang->user_language = strtolower($this->config->item('iso_639_1')[$this->uri->segments[1]]);
				
				array_shift($this->uri->segments);
				$this->uri->uri_string = implode('/', $this->uri->segments);
			}
		}
		
		parent::_set_routing();
	}
}