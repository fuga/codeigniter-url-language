<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Lang extends CI_Lang {
	
	/**
	 * User choosen language.
	 * 
	 * @var string
	 */
	public $user_language = '';
	
	/**
	 * Automatically load all the files in the $user_language directory or
	 * a specified file.
	 * 
	 * @param  string $file
	 * @return void
	 */
	public function autoload($file = NULL)
	{
		if (is_null($file))
		{
			foreach (glob(APPPATH . 'language/' . $this->user_language . '/*.php') as $file)
			{
				$this->load(basename($file), $this->user_language);
			}
		}
		else
		{
			$this->load($file, $this->user_language);
		}
	}
}