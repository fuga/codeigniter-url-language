# CodeIgniter-URL-Language
## What is this?
A simple way to have the same URL in different languages with CodeIgniter.
You can have /en/welcome and /de/wilkommen.

## How do I use it?
First you need to copy the files in the application directory.

Then you customize the `config\routes.php` file:
```php
...
$route['en/welcome']   = 'welcome/index';
$route['de/wilkommen'] = 'welcome/index';
```

Finally, in your controller function you can put this:
```php
$this->lang->autoload('user_messages');
echo $this->lang->line('user_messages_success');
```

The language is determined in the URL.

**NOTE: there is no code to check if the language has been set, so check if `$this->lang->user_language` is empty and redirect the user to `redirect('en')` for example.**